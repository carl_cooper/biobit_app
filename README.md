# README #

This app is used to display the fitness data in BioBit's team oriented fitness tracking system.
This system is a proof of concept and therefore the app is not as complete as it could be nor has it been tested on multiple devices or made compatible with multiple versions. It is designed to only work with our setup.

### Repository Information ###

* Version 1.0
* For more information and documentation see BioBit's website (http://www.calvin.edu/academic/engineering/2014-15-team10/)
* Tested and optimized for Nexus 7 (2012) (smaller phones will not display well)
* Android version - 5.0 minimum
* Uses GraphView library (https://github.com/jjoe64/GraphView) (http://www.android-graphview.org/)