package com.biobit.biobit;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Activity that displays the information for an individual player.
 *
 * The player's bio information is shown at the top, a heart rate line graph is shown in the middle,
 * two arc visuals that show the current step rate and heart rate compared to a maximum are near
 * the bottom, and the total steps and average heart rate are shown at the bottom.
 *
 * This activity asks the database for the player's bio information, all their past heart rates,
 * steps, and corresponding times, and polls the server to get updated real time data.
 *
 * Uses the GraphView library created by Jonas Gehring (jjoe64). For more information see:
 *      https://github.com/jjoe64/GraphView
 * or   http://www.android-graphview.org/
 *
 * Created by Carl Cooper
 */

public class IndividualActivity extends Activity {

    /**
     * TODO:
     *  Do cool things with the max heart rate now that I have the age
     *  Be able to edit the bio information of the player
     *  Make sure the graph displays all the information (doesn't lose it after 200 times)
     */

    private static final float MAX_SWEEP_ANGLE = 270;
    private static final int MAX_HEART_RATE = 200;
    private static final int MAX_STEP_RATE = 100;

    //URL to get player's JSON data
    private static final String URL_BIO = "/App_Bio.php?Name=";
    private static final String URL_INDIVIDUAL_CURRENT = "/App_IndivCurrentVals.php?Name=";
    private static final String URL_INDIVIDUAL_ALL = "/App_IndivHistory.php?Name=";

    private float sweepAngleStep;
    private float sweepAngleHR;

    MySurfaceView sv_heartRate;
    MySurfaceView sv_stepRate;

    //Handler and frame rate for drawing the arc visuals
    private Handler frame = new Handler();
    private static final int FRAME_RATE = 20;
    private Handler httpHandler;
    private static final int HTTP_REFRESH_RATE = 10000; //time in ms to refresh data

    //TextViews for displaying average hr and total steps
    TextView averageHrTxt;
    TextView totalStepsTxt;

    private Player player;

    private LineGraphSeries<DataPoint> hrDataSeries;
    private LineGraphSeries<DataPoint> lowerLimit;
    private LineGraphSeries<DataPoint> upperLimit;
    private LineGraphSeries<DataPoint> topLimit;
    private double lastXValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual);

        //enable the Up button to go back to Land_Page
        getActionBar().setDisplayHomeAsUpEnabled(true);

        //create the player class
        player = new Player();
        receivePlayerName();

        //display selected player
        TextView nameTxt = (TextView)findViewById(R.id.indv_name_text);
        nameTxt.setText(player.getName());

        //Initialize the views used for the arc visuals
        sv_stepRate = (MySurfaceView) findViewById(R.id.indv_stepRate_visual);
        sv_heartRate = (MySurfaceView) findViewById(R.id.indv_hr_visual);
        sv_stepRate.setSweepAngle(sweepAngleStep, MAX_STEP_RATE);
        sv_heartRate.setSweepAngle(sweepAngleHR, MAX_HEART_RATE);
        sv_stepRate.setLabel(getString(R.string.stepRate_units));
        sv_heartRate.setLabel(getString(R.string.hr_units));

        //Initialize the textViews for the average hr and total steps
        averageHrTxt = (TextView) findViewById(R.id.indv_avgHR_text);
        totalStepsTxt = (TextView) findViewById(R.id.indv_totSteps_text);

        //Initialize the view to be used for the hr graph
        GraphView graph = (GraphView) findViewById(R.id.indv_hr_graph);
        initGraphSettings(graph);
        lastXValue = 0;

        //get the player's bio data from the DB
        new GetBioInfo(URL_BIO, player.getName()).execute();

        //handler for drawing the arcs
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                initArcs();
            }
        }, 1000);

        //get the data for the player from the DB
        new GetPlayerData(URL_INDIVIDUAL_ALL, player.getName()).execute();

        httpHandler = new Handler();
        httpHandler.postDelayed(httpRunnable, HTTP_REFRESH_RATE);
    }

    private void receivePlayerName() {
        //get the name of the player from the intent
        Intent intent = getIntent();
        String name = intent.getStringExtra(getString(R.string.TAG_NAME));
        player.setName(name);
    }

    private void initGraphSettings(GraphView graph) {
        //Create a paint for the data series so that the graph looks good
        Paint mPaint = new Paint();
        mPaint.setAntiAlias(true);  //make the lines smooth
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(3.5f);
        mPaint.setColor(getResources().getColor(R.color.blue_A700));

        //Initialize the lines to show good ranges for heart rate
        lowerLimit = new LineGraphSeries<>(new DataPoint[] {
                new DataPoint(0, 100)});
        lowerLimit.setColor(getResources().getColor(R.color.GRAPH_BACKGROUND_GREEN));
        lowerLimit.setDrawBackground(true);
        lowerLimit.setBackgroundColor(getResources().getColor(R.color.GRAPH_BACKGROUND_BLUE));

        upperLimit = new LineGraphSeries<>(new DataPoint[] {
                new DataPoint(0, 170)});
        upperLimit.setColor(getResources().getColor(R.color.GRAPH_BACKGROUND_RED));
        upperLimit.setDrawBackground(true);
        upperLimit.setBackgroundColor(getResources().getColor(R.color.GRAPH_BACKGROUND_GREEN));

        topLimit = new LineGraphSeries<>(new DataPoint[] {
                new DataPoint(0, 220)  //220 so it is off the graph because 220 is max
        });
        topLimit.setColor(getResources().getColor(R.color.GRAPH_BACKGROUND_RED));
        topLimit.setDrawBackground(true);
        topLimit.setBackgroundColor(getResources().getColor(R.color.GRAPH_BACKGROUND_RED));
        graph.addSeries(topLimit);
        graph.addSeries(upperLimit);
        graph.addSeries(lowerLimit);

        //Initialize the data series
        graph.setTitle("Heart Rate");
        hrDataSeries = new LineGraphSeries<>();
        hrDataSeries.setCustomPaint(mPaint);
        graph.addSeries(hrDataSeries);

        //Initialize the view to be used for the hr graph
        graph.getViewport().setScalable(true);
        graph.getViewport().setScrollable(true);
        graph.getViewport().setMinX(0.0);
        graph.getViewport().setMaxX(100.0);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMaxY(220);
        graph.getViewport().setMinY(0);
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getGridLabelRenderer().setHorizontalAxisTitle("Time");

        //create the labeler to show the time on the x axis
        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
        @Override
        public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    try {
                        if (!(value > player.size() + 1) && (player.size() > 0)) {
                            return player.getTime((int) value-1);  //show the time for that data
                        } else {
                            return super.formatLabel(value, true);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        return "";
                    }
                } else {
                    return super.formatLabel(value, false);
                }
            }
        });
    }

    synchronized public void initArcs() {
        frame.removeCallbacks(frameUpdate);
        frame.postDelayed(frameUpdate, FRAME_RATE);
    }

    /**
     * invalidate the heart rate and step rate visuals so that they are redrawn at the FRAME_RATE
     */
    private Runnable frameUpdate = new Runnable() {
        @Override
        synchronized public void run() {
            frame.removeCallbacks(frameUpdate);
            sv_heartRate.invalidate();
            sv_stepRate.invalidate();
            frame.postDelayed(frameUpdate,FRAME_RATE);
        }
    };

    /**
     * keeps polling the server to update the data shown in the graphs
     */
    private Runnable httpRunnable = new Runnable () {
        @Override
        synchronized public void run() {
            httpHandler.removeCallbacks(httpRunnable);
            try {
                //Calling async task to get json
                new GetPlayerData(URL_INDIVIDUAL_CURRENT, player.getName()).execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
            httpHandler.postDelayed(this, HTTP_REFRESH_RATE);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_individual, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Handle presses on the action bar items
        switch (id) {
            case R.id.action_settings:
                openSettings();
                return true;
            case R.id.action_team:
                startTeamActivity();
                return true;
            case R.id.action_player:
                startIndividualActivity();
                return true;
            case R.id.action_new_player:
                startNewPlayerActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //used for the action bar buttons
    private void openSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

    //used for the action bar buttons
    private void startNewPlayerActivity() {
        Intent intent = new Intent(this, AddPlayer.class);
        startActivity(intent);
    }

    //used for the action bar buttons
    private void startTeamActivity() {
        Intent intent = new Intent(this, TeamActivity.class);
        startActivity(intent);
    }

    //used for the action bar buttons
    private void startIndividualActivity() {
        Intent intent = new Intent(this, AskForName.class);
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
        frame.removeCallbacks(frameUpdate);
        httpHandler.removeCallbacks(httpRunnable);
    }

    @Override
    public void onResume() {
        super.onResume();
        frame.postDelayed(frameUpdate, FRAME_RATE);
        httpHandler.postDelayed(httpRunnable, HTTP_REFRESH_RATE);
    }

    /**
     * Async task class to get JSON data by making HTTP call
     */
    private class GetPlayerData extends AsyncTask<Void, Void, Void> {

        private DataPoint tmpPlayerData[];
        private String url;

        public GetPlayerData(String urlFile, String name) {
            this.url = getString(R.string.URL_HUB) + urlFile + name;
        }

        @Override
        protected Void doInBackground(Void... params) {
            //Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            //Making a request to server and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {
                    //create a JSON array to store the values in
                    JSONArray jsonArray = new JSONArray(jsonStr);

                    //loop through ALL rows
                    tmpPlayerData = new DataPoint[jsonArray.length()];
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject p = jsonArray.getJSONObject(i);

                        int hr = p.getInt(getString(R.string.TAG_HR));
                        int steps = p.getInt(getString(R.string.TAG_STEPS));
                        String time = p.getString("Time");

                        player.addEntry(time.substring(11), hr, steps);

                        //store each hr in a temporary array to be drawn later
                        tmpPlayerData[i] = new DataPoint(lastXValue, hr);
                        lastXValue += 1d;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            //draw the graph with the data from the server
            try {
                //either append the data or reset the data
                if (tmpPlayerData.length == 1) {
                    //only zoom to last value if we have more than 100 values
                    if (lastXValue > 100) {
                        hrDataSeries.appendData(tmpPlayerData[0], true, 200);
                    } else {
                        hrDataSeries.appendData(tmpPlayerData[0], false, 200);
                    }
                } else {
                    hrDataSeries.resetData(tmpPlayerData);
                }

                //only zoom to last value if we have more than 100 values (100 fit on the viewport)
                if (lastXValue > 100) {
                    lowerLimit.appendData(new DataPoint(lastXValue, 100), true, 100);
                    upperLimit.appendData(new DataPoint(lastXValue, 170), true, 100);
                    topLimit.appendData(new DataPoint(lastXValue, 220), true, 100);
                } else {
                    lowerLimit.appendData(new DataPoint(lastXValue, 100), false, 100);
                    upperLimit.appendData(new DataPoint(lastXValue, 170), false, 100);
                    topLimit.appendData(new DataPoint(lastXValue, 220), false, 100);
                }

                //set the new hr angle so that the arc visuals are redrawn
                float hr_value = (float) tmpPlayerData[tmpPlayerData.length - 1].getY();
                sweepAngleHR = hr_value * MAX_SWEEP_ANGLE / MAX_HEART_RATE;
                sv_heartRate.setSweepAngle(sweepAngleHR, MAX_HEART_RATE);

                //set the new step rate so that the arc visual is redrawn
                sweepAngleStep = player.getStepRate() * MAX_SWEEP_ANGLE / MAX_STEP_RATE;
                sv_stepRate.setSweepAngle(sweepAngleStep, MAX_STEP_RATE);

                //display the current steps and the average heart rate
                totalStepsTxt.setText("Total: " + Integer.toString(player.getCurrSteps()));
                averageHrTxt.setText("AVG HR: " + Integer.toString(player.getAvgHr()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Async task class to get player info by making HTTP call
     */
    private class GetBioInfo extends AsyncTask<Void, Void, Void> {

        private String age;
        private String height;
        private String weight;
        private String position;
        private String url;

        public GetBioInfo(String urlFile, String name) {
            this.url = getString(R.string.URL_HUB) + urlFile + name;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            //Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            //Making a request to server and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {

                    //create a JSON array to store the values in
                    JSONArray jsonArray = new JSONArray(jsonStr);

                    //looping through information -> there should only be one
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject p = jsonArray.getJSONObject(i);

                        age = p.getString("Age");
                        height = p.getString("Height");
                        weight = p.getString("Weight");
                        position = p.getString("Position");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            //update the headers showing bio information
            TextView ageTxt = (TextView) findViewById(R.id.indv_age_text);
            TextView heightTxt = (TextView) findViewById(R.id.indv_height_text);
            TextView weightTxt = (TextView) findViewById(R.id.indv_weight_text);
            TextView posTxt = (TextView) findViewById(R.id.indv_pos_text);
            ageTxt.setText(age);
            heightTxt.setText(height);
            weightTxt.setText(weight + " lbs");
            posTxt.setText(position);
        }
    }
}
