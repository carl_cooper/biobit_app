package com.biobit.biobit;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Carl Cooper on 3/25/2015.
 *
 * This file contains the code to create an arc visual on a surface view.
 * It draws a dark arc for the current value and a lighter arc as a "background" for comparison
 * as well as writes out the value and dimensions (ex bpm).
 */
public class MySurfaceView extends View {

    private static final float START_ANGLE = 135;
    private static final float MAX_SWEEP_ANGLE = 270;
    private static final float ARC_WIDTH = 30;
    private static final int Y_OFFSET = 20;

    private static RectF oval;
    private static float centerX;
    private static float centerY;

    private static final int PAINT_TEXT_SIZE = 50;
    private static final int PAINT_TEXT_STROKE_SIZE = 2;

    private float sweepAngle;
    private int currentVal;
    private String label = "";
    private Paint p;
    private Paint pText;

    public MySurfaceView(Context context) {
        super(context);
        init();
    }

    public MySurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MySurfaceView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        //set up paint for the arcs
        p = new Paint();
        p.setStrokeWidth(ARC_WIDTH);
        p.setStyle(Paint.Style.STROKE);
        p.setAntiAlias(true);   //makes the edges smoother

        //set up paint for the text
        pText = new Paint();
        pText.setStrokeWidth(PAINT_TEXT_STROKE_SIZE);
        pText.setTextSize(PAINT_TEXT_SIZE);
        pText.setStyle(Paint.Style.FILL);
        pText.setAntiAlias(true);
        pText.setColor(getResources().getColor(R.color.black));
        pText.setTextAlign(Paint.Align.CENTER);
    }

    private void calcDimensions(Canvas canvas) {
        //calculate the dimensions of the arc to be drawn
        float width = canvas.getWidth();
        float height = canvas.getHeight();
        float radius;
        if (width > height) {
            radius = height / 2;
        } else {
            radius = width / 2;
        }
        centerX = width/2;
        centerY = height/2 + Y_OFFSET;
        oval = new RectF(centerX - radius,
                centerY - radius,
                centerX + radius,
                centerY + radius);
    }

    @Override
    synchronized public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //calculate the center the first time
        if (centerX == 0 && centerY == 0) {
            calcDimensions(canvas);
        }

        //paint a background color
//        p.setAlpha(10);
//        canvas.drawColor(getResources().getColor(R.color.green_background));
//        p.setAlpha(255);

        //paint an arc for the visual
        paintArc(canvas, sweepAngle);

        //draw the value in text
        String txt = Integer.toString(currentVal);
        canvas.drawText(txt, centerX, centerY, pText);

        //draw the label for the text
        canvas.drawText(label, centerX, centerY+50, pText);
    }

    private void paintArc(Canvas canvas, float angle) {
        //draw the current value arc
        if (currentVal < 100) {
            p.setColor(getResources().getColor(R.color.ARC_FOREGROUND_BLUE));
            canvas.drawArc(oval, START_ANGLE, angle, false, p);
            //fill in the rest as a "background" color to give comparison
            p.setColor(getResources().getColor(R.color.ARC_BACKGROUND_BLUE));
            canvas.drawArc(oval, START_ANGLE+angle, MAX_SWEEP_ANGLE-angle, false, p);
        } else if (currentVal < 170) {
            p.setColor((getResources().getColor(R.color.ARC_FOREGROUND_GREEN)));
            canvas.drawArc(oval, START_ANGLE, angle, false, p);
            //fill in the rest as a "background" color to give comparison
            p.setColor(getResources().getColor(R.color.ARC_BACKGROUND_GREEN));
            canvas.drawArc(oval, START_ANGLE+angle, MAX_SWEEP_ANGLE-angle, false, p);
        } else {
            p.setColor((getResources().getColor(R.color.ARC_FOREGROUND_RED)));
            canvas.drawArc(oval, START_ANGLE, angle, false, p);
            //fill in the rest as a "background" color to give comparison
            p.setColor(getResources().getColor(R.color.ARC_BACKGROUND_RED));
            canvas.drawArc(oval, START_ANGLE+angle, MAX_SWEEP_ANGLE-angle, false, p);
        }

    }

    public void setSweepAngle(float f, int maxVal) {
        sweepAngle = f;
        currentVal = Math.round(f * maxVal / MAX_SWEEP_ANGLE);
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
