package com.biobit.biobit;

/**
 * Created by Carl Cooper on 4/27/2015.
 *
 * This file contains a class to store an entry coming from the data.
 * An entry contains a time (string), heart rate, and and steps.
 *
 * Used in the Player class in the history array.
 */
public class DataTriple {

    private String time;  //or maybe date
    private int hr;
    private int steps;

    public DataTriple(String t, int hr, int st) {
        this.time = t;
        this.hr = hr;
        this.steps = st;
    }

    public String getTime() {
        return time;
    }

    public int getHr() {
        return hr;
    }

    public int getSteps() {
        return steps;
    }
}
