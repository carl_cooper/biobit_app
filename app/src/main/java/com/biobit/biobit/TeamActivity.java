package com.biobit.biobit;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Activity to display team's current heart rate and steps.
 * Polls server and updates ListView to show real time data.
 * Calculates averages for heart rate and steps and highlights when a heart rate is too high
 *
 * Create by Carl Cooper
 */

/**
 * TODO:
 *  Be able to delete players
 */
public class TeamActivity extends Activity {

    /** --- Define Global variables --- */
    //URL to get players JSON
    protected static String URL = "http://192.168.10.1/App_AllCurrentVals.php";
    private static final int HTTP_REFRESH_RATE = 10000;  //in ms -> 10s

    // Hashmap for ListView
    private ArrayList<HashMap<String, String>> playerList;

    // ListView that displays the data
    private  ListView lv;
    private MyPerformanceAdapter adapter;

    //Average values for the team
    private double avg_hr = 0;
    private double avg_steps = 0;

    // Timer for refreshing the data
    private Handler httpHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        //enable the app icon as the Up button to go back to Land_Page
        this.getActionBar().setDisplayHomeAsUpEnabled(true);

        //create the ArrayList to store the players' data
        playerList = new ArrayList<>();

        lv = (ListView)findViewById(R.id.list);
        adapter = new MyPerformanceAdapter(TeamActivity.this, playerList);
        lv.setAdapter(adapter);

        //ListView on item click listener to open individual player page
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //getting name value from selected ListItem
                String name = ((TextView)view.findViewById(R.id.list_name)).getText().toString();

                //Starting single player activity
                Intent intent = new Intent(getApplicationContext(), IndividualActivity.class);
                intent.putExtra(getString(R.string.TAG_NAME), name);
                startActivity(intent);
            }
        });

        //Get the data from the server and keep updating it
        httpHandler = new Handler();
        httpHandler.postDelayed(httpRunnable, 100);
    }

    /**
     * used to keep polling the server for the team's current data
     */
    private Runnable httpRunnable = new Runnable () {
        @Override
        synchronized public void run() {
            httpHandler.removeCallbacks(httpRunnable);
            try {
                //Calling async task to get json
                new GetPlayers().execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
            httpHandler.postDelayed(this, HTTP_REFRESH_RATE);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_team, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Handle presses on the action bar items
        switch (id) {
            case R.id.action_settings:
                openSettings();
                return true;
            case R.id.action_player:
                startIndividualActivity();
                return true;
            case R.id.action_new_player:
                startNewPlayerActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        httpHandler.removeCallbacks(httpRunnable);
    }

    @Override
    public void onResume() {
        super.onResume();
        httpHandler.postDelayed(httpRunnable, 100);
    }

    private void openSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

    private void startNewPlayerActivity() {
        Intent intent = new Intent(this, AddPlayer.class);
        startActivity(intent);
    }

    //used for the action bar buttons
    private void startIndividualActivity() {
        Intent intent = new Intent(this, AskForName.class);
        startActivity(intent);
    }

    /**
     * Async task class to get JSON by making HTTP call
     */
    private class GetPlayers extends AsyncTask<Void, Void, Void> {

        //only show the averages to one decimal place
        DecimalFormat decimalFormat = new DecimalFormat("#");

        @Override
        protected Void doInBackground(Void... params) {
            //Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            //Making a request to server and getting response
            String jsonStr = sh.makeServiceCall(URL, ServiceHandler.GET);

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {
                    //reset the average heart rate and  steps to be recalculated for this data set
                    avg_hr = 0;
                    avg_steps = 0;

                    //create a JSON array to store the values in
                    JSONArray jsonArray = new JSONArray(jsonStr);

                    //looping through ALL players (ids)
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject p = jsonArray.getJSONObject(i);

                        String name = p.getString(getString(R.string.TAG_NAME));
                        String hr = Integer.toString(p.getInt(getString(R.string.TAG_HR)));
                        String steps = Integer.toString(p.getInt(getString(R.string.TAG_STEPS)));
                        String time = p.getString("Time");
                        int hour = Integer.parseInt(time.substring(time.length()-8, time.length()-6));
                        int mins = Integer.parseInt(time.substring(time.length()-5, time.length()-3));
                        int sec = Integer.parseInt(time.substring(time.length()-2));
                        int time1 = convertToSeconds(hour, mins, sec);

                        //recalculate the averages
                        avg_hr = calcCMA(Double.parseDouble(hr), i, avg_hr);
                        avg_steps = calcCMA(Double.parseDouble(steps), i, avg_steps);

                        //calculate if the player is connected or not
                        String isConnected = calcConnection(time1);

                        //tmp hashmap for single player
                        HashMap<String, String> player = new HashMap<>();

                        //adding each child node to HashMap key => value
                        player.put(getString(R.string.TAG_NAME), name);
                        player.put(getString(R.string.TAG_HR), hr);
                        player.put(getString(R.string.TAG_STEPS), steps);
                        player.put(getString(R.string.TAG_CONNECTION), isConnected);

                        //if the list is not full (first time adding players to the list)
                        if (playerList.size() != jsonArray.length()) {
                            playerList.add(player);
                        } else {  //update the list that already exists
                            playerList.set(i, player);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            //update parsed JSON data into ListView using custom adapter
            adapter = (MyPerformanceAdapter) lv.getAdapter();
            if (adapter == null) {
                adapter = new MyPerformanceAdapter(TeamActivity.this, playerList);
                lv.setAdapter(adapter);
            } else {
                adapter.notifyDataSetChanged();
            }

            //update the headers showing the averages
            TextView hr_tv = (TextView)findViewById(R.id.header_avg_heartRate);
            hr_tv.setText(decimalFormat.format(avg_hr));
            TextView step_tv = (TextView)findViewById(R.id.header_avg_steps);
            step_tv.setText(decimalFormat.format(avg_steps));
        }

        /**
         * convertToSeconds - converts a time broken up by hours, mins, secs into seconds
         * @param hour - hour portion of time (24 hour clock)
         * @param mins - minute portion of the time
         * @param sec - second portion of the time
         * @return int that is the time in total seconds
         */
        private int convertToSeconds(int hour, int mins, int sec) {
            return (hour*3600) + (mins*60) + sec;
        }

        /**
         * calcConnection - calculates the time since the data for that player has been updated
         *      - if the player is connected, it displays the connected image
         *      - if 20 seconds have passed, it makes the connected image disappear
         * @param time1 - the time value (in seconds) for the last time the server was updated
         * @return string indicating if there is a connection or not (t or f)
         */
        private String calcConnection(int time1) {
            Calendar c = Calendar.getInstance();
            int timeNow = convertToSeconds(c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), c.get(Calendar.SECOND));

            if (Math.abs(timeNow-time1) < 20) {
                return "t";
            } else {
                return "f";
            }
        }
    }

    /**
     * calcCMA - calculates the cumulative moving average to give the average of the team
     *      CMA[n+1] = (x[n+1] + n*CMA[n]) / (n + 1)
     * @param x - current value
     * @param n - - number of players calculated in previous average
     * @param avg - previous average
     * @return int - new team average
     */
    private double calcCMA(double x, int n, double avg) {
        return (x + n*avg)/(n+1);
    }
}
