package com.biobit.biobit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.List;

/**
 * Created by Carl Cooper on 3/25/2015.
 *
 * ServiceHandler Class - handles sending and receiving an HTTP request to the server
 * Used to receive data from the server
 *
 * Requires - IP address of server and specific page to send request to and method (GET or POST)
 * Returns - String response from the server
 */
public class ServiceHandler {

        static String response = null;
        public final static int GET = 1;
        public final static int POST = 2;

        public ServiceHandler() {

        }

    /**
     * Making service call
     * @param url - url to make request
     * @param method - http request method
     * @return String response from server
     */
        public String makeServiceCall(String url, int method) {
            return this.makeServiceCall(url, method, null);
        }

    /**
     * Making service call
     * @param url - url to make request
     * @param method - http method
     * @param params - http request params
     * @return String response from server
     */
        public String makeServiceCall(String url, int method, List<NameValuePair> params) {
            try {
                //http client
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpEntity httpEntity;
                HttpResponse httpResponse = null;

                //Checking http request method type
                if (method == POST) {
                    HttpPost httpPost = new HttpPost(url);
                    //adding post params
                    if (params != null) {
                        httpPost.setEntity(new UrlEncodedFormEntity(params));
                    }

                    httpResponse = httpClient.execute(httpPost);
                } else if (method == GET) {
                    //appending params to url
                    if (params != null) {
                        String paramString = URLEncodedUtils.format(params, "utf-8");
                        url += "?" + paramString;
                    }
                    HttpGet httpGet = new HttpGet(url);

                    httpResponse = httpClient.execute(httpGet);
                }
                if (httpResponse != null) {
                    httpEntity = httpResponse.getEntity();
                    response = EntityUtils.toString(httpEntity);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response;
        }
}
