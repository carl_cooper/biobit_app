package com.biobit.biobit;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

/**
 * Activity that the user sees when they open the app and are directed to the most often as the
 * "home" page.
 * Displays the coach, school, and team name and allows the user to access every other page to give
 * them the optimal navigation.
 *
 * Created by Carl Cooper
 */
public class Land_Page extends Activity {

    private static final String PREFS_NAME = "MyPrefsFile";
    private static final int RUN_CODE = 1;

    private TextView coach;
    private TextView school;
    private TextView team;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_land__page);

        //initialize view used for displaying team information
        coach = (TextView) findViewById(R.id.land_coach);
        school = (TextView) findViewById(R.id.land_school);
        team = (TextView) findViewById(R.id.land_team);

        //get the coach, team, school names from the server and display them
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        boolean firstRun = settings.getBoolean("firstRun", true);
        if (firstRun) {
            startActivityForResult(
                    new Intent(this, FirstRun.class), RUN_CODE);
        }

        insertData();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_land__page, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Handle presses on the action bar items
        switch (id) {
            case R.id.action_settings:
                openSettings();
                return true;
            case R.id.action_team:
                startTeamActivity();
                return true;
            case R.id.action_player:
                startIndividualActivity();
                return true;
            case R.id.action_new_player:
                startNewPlayerActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RUN_CODE) {
            if (resultCode == RESULT_OK) {
                //set the firstRun flag to false
                SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("firstRun", false);
                editor.apply();

                //now put the data into the table
                insertData();
            }
        }
    }

    private void insertData() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 1);
        String coachName = settings.getString("name", "");
        String schoolName = settings.getString("school", "");
        String teamName = settings.getString("team", "");

        coach.append("  " + coachName);
        school.append("  " + schoolName);
        team.append("  " + teamName);
    }


    private void openSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

    private void startNewPlayerActivity() {
        Intent intent = new Intent(this, AddPlayer.class);
        startActivity(intent);
    }

    //used for the action bar buttons
    private void startTeamActivity() {
        Intent intent = new Intent(this, TeamActivity.class);
        startActivity(intent);
    }

    //used for the buttons in the activity
    public void startTeamActivity(View view) {
        Intent intent = new Intent(this, TeamActivity.class);
        startActivity(intent);
    }

    //used for the action bar buttons
    private void startIndividualActivity() {
        Intent intent = new Intent(this, AskForName.class);
        startActivity(intent);
    }

    //used for the buttons in the activity
    public void startIndividualActivity(View view) {
        Intent intent = new Intent(this, AskForName.class);
        startActivity(intent);
    }
}
