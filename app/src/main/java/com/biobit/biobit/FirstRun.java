package com.biobit.biobit;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

/**
 * Activity to set up the app. Shown when the app runs for the very first time.
 * The user is prompted to enter the coach, school, and team name and these settings are saved in
 * the app's preferences. These settings are displayed on the Land Page on subsequent starts.
 * When the user enters the information, they are sent to the Land Page.
 *
 * Created by Carl Cooper
 */

public class FirstRun extends Activity {

    private static final String SETTINGS_NAME = "MyPrefsFile";

    private EditText nameText;
    private EditText schoolText;
    private EditText teamText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_run);

        nameText = (EditText) findViewById(R.id.add_coach_name_editText);
        schoolText = (EditText) findViewById(R.id.add_school_editText);
        teamText = (EditText) findViewById(R.id.add_team_editText);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_first_run, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void SaveSettings(View view) {
        //get the text entered in the editTexts
        String coach = nameText.getText().toString();
        String school = schoolText.getText().toString();
        String team = teamText.getText().toString();

        //store them in the SharedPreferences
        SharedPreferences settings = getSharedPreferences(SETTINGS_NAME, 1);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("name", coach);
        editor.putString("school", school);
        editor.putString("team", team);
        editor.apply();

        //go back to the land page
        setResult(RESULT_OK);
        finish();
    }
}
