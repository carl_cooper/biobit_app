package com.biobit.biobit;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * Activity to add new players to the database.
 * User is prompted to enter the information for a player and when a button is pressed it sends
 * the information to the server. There the information is put into a Bio table and the appropriate
 * rows and tables are created and initialized on the database.
 *
 * Created by Carl Cooper
 */

public class AddPlayer extends Activity {

    private EditText nameText;
    private EditText ageText;
    private EditText heightText_ft;
    private EditText heightText_in;
    private EditText weightText;
    private EditText positionText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_player);

        //enable the app icon as the Up button to go back to Land_Page
        this.getActionBar().setDisplayHomeAsUpEnabled(true);

        nameText = (EditText) findViewById(R.id.bio_name_editText);
        ageText = (EditText) findViewById(R.id.bio_age_editText);
        heightText_ft = (EditText) findViewById(R.id.bio_height_ft_editText);
        heightText_in = (EditText) findViewById(R.id.bio_height_in_editText);
        weightText = (EditText) findViewById(R.id.bio_weight_editText);
        positionText = (EditText) findViewById(R.id.bio_position_editText);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_player, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Handle presses on the action bar items
        switch (id) {
            case R.id.action_settings:
                openSettings();
                return true;
            case R.id.action_team:
                startTeamActivity();
                return true;
            case R.id.action_player:
                startIndividualActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

    //used for the action bar buttons
    private void startTeamActivity() {
        Intent intent = new Intent(this, TeamActivity.class);
        startActivity(intent);
    }

    //used for the action bar buttons
    private void startIndividualActivity() {
        Intent intent = new Intent(this, AskForName.class);
        startActivity(intent);
    }

    public void createNewPlayer(View view) {
        //get the information that has been entered
        String name = nameText.getText().toString();
        String age = ageText.getText().toString();
        String height = heightText_ft.getText().toString() + "ft" + " " +
                heightText_in.getText().toString() + "in";
        String weight = weightText.getText().toString();
        String position = positionText.getText().toString();
        try {
            new CreateNewPlayer(name,age, height, weight, position).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Toast.makeText(this, "Created new player: " + name, Toast.LENGTH_SHORT).show();

        //clear the fields so that a new player can be entered
        nameText.setText("");
        ageText.setText("");
        heightText_ft.setText("");
        heightText_in.setText("");
        weightText.setText("");
        positionText.setText("");
    }


    private class CreateNewPlayer extends AsyncTask<Void, Void, Void> {

        private String url = getString(R.string.URL_HUB) + "/Players_Bio.php";
        ArrayList<NameValuePair> httpParams;

        public CreateNewPlayer(String nm, String age, String ht, String wt, String pos) {
            httpParams = new ArrayList<>();
            httpParams.add(new BasicNameValuePair(getString(R.string.Name), nm));
            httpParams.add(new BasicNameValuePair(getString(R.string.age), age));
            httpParams.add(new BasicNameValuePair(getString(R.string.Height), ht));
            httpParams.add(new BasicNameValuePair(getString(R.string.Weight), wt));
            httpParams.add(new BasicNameValuePair(getString(R.string.Position), pos));
        }

        @Override
        protected Void doInBackground(Void... params) {
            //Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            //Making a request to server and getting response
            String serverResponse = sh.makeServiceCall(url, ServiceHandler.GET, httpParams);
            Log.d("AddPlayer:", serverResponse);
            return null;
        }
    }
}
