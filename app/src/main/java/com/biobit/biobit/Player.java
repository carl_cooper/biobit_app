package com.biobit.biobit;

import java.util.ArrayList;

/**
 * Created by Carl on 3/7/2015.
 *
 * Class to store all the information about a player including current and past data.
 */
public class Player {
    private String name;
    private String position;
    private double height;
    private double weight;
    private int currHR;
    private int currSteps;
    private String currTime;
    private ArrayList<DataTriple> history;
    private int avgHR;
    private int stepRate;

    //default constructor
    public Player() {
        name = null;
        position = null;
        height = 0;
        weight = 0;
        currHR = 0;
        currSteps = 0;
        currTime = null;
        history = new ArrayList<>();
        stepRate = 0;
    }

    //get methods
    public String getName() {
        return this.name;
    }
    
    public String getPosition() {
        return this.position;
    }

    public double getHeight() {
        return this.height;
    }

    public double getWeight() {
        return this.weight;
    }

    public int getCurrHR() {
        return this.currHR;
    }

    public int getCurrSteps() {
        return this.currSteps;
    }

    public String getCurrTime() {
        return this.currTime;
    }

    public String getTime(int index) {
        return history.get(index).getTime();
    }

    public int getStepRate() {
        return this.stepRate;
    }

    public int getAvgHr() {
        return this.avgHR;
    }

    public DataTriple getEntry (int index) {
        return history.get(index);
    }

    //set methods
    public void setName(String name) {
        this.name = name;
    }

    public void setPosition(String pos) {
        this.position = pos;
    }

    public void setHeight(double h) {
        this.height = h;
    }

    public void setWeight(double w) {
        this.weight = w;
    }

    public void addEntry(String t, int h, int s) {
        this.history.add(new DataTriple(t, h, s));
        this.currHR = h;
        this.currSteps = s;
        this.currTime = t;

        if (history.size() > 5) {
            calcStepRate();
            calcAvgHR();
        }
    }

    private void calcAvgHR() {
        int tempHR = 0;
        for(int i = 1; i < 6; i++) {
            tempHR += history.get(history.size() - i).getHr();
        }
        avgHR = tempHR / 5;
    }

    //stepRate = change in steps / change in time
    private void calcStepRate() {
        //find the steps and time for 3 entries ago
        int step1 = history.get(history.size()-4).getSteps();
        String time1 = history.get(history.size()-4).getTime();  //compare to 3 entries ago
        int hour = Integer.parseInt(time1.substring(time1.length()-8, time1.length()-6));
        int mins = Integer.parseInt(time1.substring(time1.length()-5, time1.length()-3));
        int sec = Integer.parseInt(time1.substring(time1.length()-2));
        int seconds1 = convertToSeconds(hour, mins, sec);

        //find the current steps and time
        //step2 = currSteps
        String time2 = history.get(history.size()-1).getTime();  //most recent entry
        hour = Integer.parseInt(time2.substring(time2.length()-8, time2.length()-6));
        mins = Integer.parseInt(time2.substring(time2.length()-5, time2.length()-3));
        sec = Integer.parseInt(time2.substring(time2.length()-2));
        int seconds2 = convertToSeconds(hour, mins, sec);

        int timeChange = seconds2 - seconds1;
        //if the time change is negative (in the future) or more than 5 minutes return with 0 stepRate
        if (timeChange <= 0 || timeChange > 300) {
            stepRate = 0;
            return;
        }

        int stepChange = currSteps - step1;
        if (stepChange < 0) {
            stepChange = 0;
        }
        stepRate = 60 * stepChange / timeChange;
    }

    public int size() {
        return history.size();
    }

    /**
     * convertToSeconds - converts a time broken up by hours, mins, secs into seconds
     * @param hour - hour portion of time (24 hour clock)
     * @param mins - minute portion of the time
     * @param sec - second portion of the time
     * @return int that is the time in total seconds
     */
    private int convertToSeconds(int hour, int mins, int sec) {
        return (hour*3600) + (mins*60) + sec;
    }
}
