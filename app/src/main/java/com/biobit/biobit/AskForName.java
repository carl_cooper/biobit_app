package com.biobit.biobit;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Activity to select the name to show in the Individual Activity if no name is given.
 * The names of the players on the team are obtained from the database and displayed on in a
 * ListView that is clickable. The user can also type in a name to display, as long as it is a name
 * in the database. When a name is selected, the name is passed onto the Individual Activity in an
 * intent.
 *
 * Created by Carl Cooper
 */
public class AskForName extends Activity {
    protected final static String URL = "http://192.168.10.1/App_AllCurrentVals.php";

    private ListView lv;
    private ArrayList<String> playerNameList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_for_name);

        playerNameList = new ArrayList<>();

        //enable the Up button to go back to Land_Page
        getActionBar().setDisplayHomeAsUpEnabled(true);

        lv = (ListView) findViewById(R.id.player_list);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String name = (String) parent.getItemAtPosition(position);

                Intent intent = new Intent(getApplicationContext(), IndividualActivity.class);
                intent.putExtra(getString(R.string.TAG_NAME), name);
                startActivity(intent);
            }
        });
        new GetPlayerNames().execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ask_for_name, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Handle presses on the action bar items
        switch (id) {
            case R.id.action_settings:
                openSettings();
                return true;
            case R.id.action_team:
                startTeamActivity();
                return true;
            case R.id.action_new_player:
                startNewPlayerActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

    private void startNewPlayerActivity() {
        Intent intent = new Intent(this, AddPlayer.class);
        startActivity(intent);
    }

    //used for the action bar buttons
    private void startTeamActivity() {
        Intent intent = new Intent(this, TeamActivity.class);
        startActivity(intent);
    }

    public void startIndvActivity(View view) {
        String name = ((EditText)findViewById(R.id.edit_name)).getText().toString();

        if (playerNameList.contains(name)) {
            //Starting single player activity
            Intent intent = new Intent(getApplicationContext(), IndividualActivity.class);
            intent.putExtra("Name", name);
            startActivity(intent);
        } else {
            Toast.makeText(this, name + " is not on your team", Toast.LENGTH_LONG).show();
            ((EditText) findViewById(R.id.edit_name)).setText("");
        }
    }

    private class GetPlayerNames extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            //Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            //Making a request to server and getting response
            String jsonStr = sh.makeServiceCall(URL, ServiceHandler.GET);

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {
                    //create a JSON array to store the values in
                    JSONArray jsonArray = new JSONArray(jsonStr);

                    //looping through ALL players (ids)
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject p = jsonArray.getJSONObject(i);

                        String name = p.getString(getString(R.string.TAG_NAME));

                        playerNameList.add(name);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            final ArrayAdapter<String> adapter = new ArrayAdapter<>(
                    AskForName.this, R.layout.select_player_list_item, playerNameList);
            lv.setAdapter(adapter);
        }
    }
}
