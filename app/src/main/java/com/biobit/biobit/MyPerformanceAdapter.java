package com.biobit.biobit;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Carl Cooper on 3/25/2015.
 *
 * This is a custom adapter that increases performance and will create the listView of the data
 * It will change the background of the row if the heart rate is too high
 */
public class MyPerformanceAdapter extends ArrayAdapter<HashMap<String, String>> {
    private static final String TAG_NAME = "Name";
    private static final String TAG_HR = "Heart Rate";
    private static final String TAG_STEPS = "Steps";
    private static final String TAG_CONNECTION = "isConnected";

    private final Activity context;
    private final ArrayList<HashMap<String, String>> players;

    private static final int HEART_RATE_DANGER_LINE = 170;

    static class ViewHolder {
        public TextView name_txt;
        public TextView hr_txt;
        public TextView step_txt;
        public ImageView connection_image;
    }

    public MyPerformanceAdapter(Activity context, ArrayList<HashMap<String, String>> players) {
        super(context, R.layout.list_item, players);
        this.context = context;
        this.players = players;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        //reuse views
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.list_item, null);
            //configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.name_txt = (TextView)rowView.findViewById(R.id.list_name);
            viewHolder.hr_txt = (TextView)rowView.findViewById(R.id.list_heartRate);
            viewHolder.step_txt = (TextView)rowView.findViewById(R.id.list_steps);
            viewHolder.connection_image = (ImageView)rowView.findViewById(R.id.list_connected_image);
            rowView.setTag(viewHolder);
        }

        //fill data
        ViewHolder holder = (ViewHolder)rowView.getTag();
        HashMap<String, String> s = players.get(position);
        holder.name_txt.setText(s.get(TAG_NAME));
        holder.hr_txt.setText(s.get(TAG_HR));
        holder.step_txt.setText(s.get(TAG_STEPS));
        if (Integer.parseInt(s.get(TAG_HR)) > HEART_RATE_DANGER_LINE) {
            rowView.findViewById(R.id.list_heartRate).setBackgroundColor(Color.RED);
        } else {
            rowView.findViewById(R.id.list_heartRate).setBackgroundColor(0xEEEEEE);
        }
        if (s.get(TAG_CONNECTION).equals("t")) {
            holder.connection_image.setVisibility(View.VISIBLE);
        } else {
            holder.connection_image.setVisibility(View.INVISIBLE);
        }

        return rowView;
    }
}
